To install project you need docker.
1. clone the project
`git clone git@gitlab.com:crazykoha/symfony-learning.git `
2. build docker containers
`docker compose up -d --build`
3. create .env file `cp .env.example .env`
4. create test db `docker exec php php bin/console doctrine:database:create`
by default DB name is news_test. But if you changed the default DB name then test DB name should be {default_db_name}_test