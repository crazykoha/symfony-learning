<?php

namespace App\Service;

use App\Entity\User;
use App\Request\Dto\UserUpdateDto;
use Doctrine\ORM\EntityManagerInterface;

final readonly class UserProfileService
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function update(User $user, UserUpdateDto $dto): User
    {
        $user->setFirstName($dto->first_name);
        $user->setLastName($dto->last_name);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
