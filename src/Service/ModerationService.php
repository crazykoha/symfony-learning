<?php

namespace App\Service;

use App\Entity\BanReason;
use App\Entity\Post;
use App\Entity\PostType;
use App\Entity\User;
use App\Request\Dto\PostStoreDto;
use App\Request\Dto\PostUpdateDto;
use Doctrine\ORM\EntityManagerInterface;

final class ModerationService
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function setRole(User $user, string $role): void
    {
        $user->setRole($role);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function banUser(User $user, int $banReasonId): void
    {
        $banReason = $this->entityManager->getRepository(BanReason::class)->find($banReasonId);
        $user->setBanReason($banReason);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
