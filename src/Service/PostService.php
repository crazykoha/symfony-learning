<?php

namespace App\Service;

use App\Entity\Post;
use App\Entity\PostType;
use App\Entity\User;
use App\Request\Dto\PostStoreDto;
use App\Request\Dto\PostUpdateDto;
use Doctrine\ORM\EntityManagerInterface;

final class PostService
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function getPosts(int $page)
    {
        $repository = $this->entityManager->getRepository(Post::class);

        return $repository->getPaginatedPosts($page);
    }

    public function create(PostStoreDto $postStoreDto, User $author): void
    {
        $postType = $this->entityManager->getRepository(PostType::class)->find($postStoreDto->post_type_id);
        $post = new Post();
        $post->setTitle($postStoreDto->title);
        if ('video' !== $postType->getName()) {
            $bodyLength = strlen($postStoreDto->body);
            $contextLength = $bodyLength > 300 ? 150 : ($bodyLength / 2);
            $context = substr($postStoreDto->body, 0, $contextLength);
        } else {
            $context = $postStoreDto->context;
        }
        $post->setContext($context);
        $post->setBody($postStoreDto->body);
        $post->setAuthor($this->entityManager->getRepository(User::class)->find($author->getId()));
        $post->setType($postType);
        $this->entityManager->persist($post);
        $this->entityManager->flush();
    }

    public function update(Post $post, PostUpdateDto $dto, User $author): Post
    {
        $post->setTitle($dto->title);
        $post->setContext($dto->context);
        $post->setBody($dto->body);
        $post->setType($this->entityManager->getRepository(PostType::class)->find($dto->post_type_id));
        $post->setUpdatedBy($this->entityManager->getRepository(User::class)->find($author->getId()));
        $this->entityManager->persist($post);
        $this->entityManager->flush();

        return $post;
    }
}
