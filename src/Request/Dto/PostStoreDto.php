<?php

namespace App\Request\Dto;

use App\Constraint\Exists;
use App\Constraint\RequiredIfTypeVideo;
use App\Entity\PostType;
use Symfony\Component\Validator\Constraints as Assert;

class PostStoreDto
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Type('string')]
        #[Assert\Length(max: 150)]
        public string $title,

        #[Assert\Type('string')]
        #[RequiredIfTypeVideo]
        public string $context,

        #[Assert\NotBlank]
        #[Assert\Type('string')]
        public string $body,

        #[Assert\NotBlank]
        #[Exists(target: PostType::class)]
        public int $post_type_id,
    ) {
    }
}
