<?php

namespace App\Request\Dto;

use App\Constraint\Exists;
use App\Entity\PostType;
use Symfony\Component\Validator\Constraints as Assert;

class PostUpdateDto
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Length(max: 500)]
        public string $title,

        #[Assert\NotBlank]
        #[Assert\Length(max: 500)]
        public string $context,

        #[Assert\NotBlank]
        #[Assert\Length(max: 500)]
        public string $body,

        #[Exists(target: PostType::class)]
        public int $post_type_id,
    ) {
    }
}