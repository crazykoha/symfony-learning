<?php

namespace App\Request\Dto;

use App\Enums\UserRoleEnum;
use Symfony\Component\Validator\Constraints as Assert;

class ModerationApproveDto
{
    public function __construct(
        #[Assert\Choice(choices: [UserRoleEnum::Author->value, UserRoleEnum::Moderator->value, UserRoleEnum::Admin->value])]
        public string $role,
    ) {
    }
}
