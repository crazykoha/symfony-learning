<?php

namespace App\Request\Dto;

use App\Constraint\Exists;
use App\Entity\BanReason;
use Symfony\Component\Validator\Constraints as Assert;

class UserUpdateDto
{
    public function __construct(
        #[Assert\Length(max: 255)]
        public string $first_name,
        #[Assert\Length(max: 255)]
        public string $last_name,
    ) {
    }
}
