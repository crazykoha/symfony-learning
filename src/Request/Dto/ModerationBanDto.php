<?php

namespace App\Request\Dto;

use App\Constraint\Exists;
use App\Entity\BanReason;

class ModerationBanDto
{
    public function __construct(
        #[Exists(BanReason::class)]
        public int $ban_reason_id,
    ) {
    }
}
