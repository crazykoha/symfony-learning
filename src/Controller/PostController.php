<?php

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;
use App\Request\Dto\PostStoreDto;
use App\Request\Dto\PostUpdateDto;
use App\Response\PostCollection;
use App\Response\PostResource;
use App\Service\PostService;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api', name: 'api_')]
final class PostController extends AbstractController
{
    public function __construct(
        private readonly PostService $postService,
        private readonly PostRepository $postRepository,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/posts', name: 'posts', methods: ['GET'])]
    public function index(Request $request): JsonResponse
    {
        $data = $this->postRepository->getPaginatedPosts($this->serializer->decode($request->getContent(), 'json')['page']);
        $encodedData = $this->serializer->encode((new PostCollection())->paginatedCollection($data), 'json');

        return new JsonResponse($encodedData);
    }

    #[Route('/posts/{id}', name: 'post', methods: ['GET'])]
    public function show(#[MapEntity(Post::class)] $post): JsonResponse
    {
        return new JsonResponse($this->serializer->encode((new PostResource())->item($post), 'json'));
    }

    #[Route('/posts', name: 'create_post', methods: 'POST')]
    #[IsGranted('create')]
    public function store(
        #[MapRequestPayload()] PostStoreDto $postStoreDto,
    ): JsonResponse {
        $this->postService->create($postStoreDto, $this->getUser());

        return new JsonResponse(null, Response::HTTP_CREATED);
    }

    #[Route('/posts/{id}', name: 'update_post', methods: ['PATCH'])]
    public function update(
        #[MapRequestPayload] PostUpdateDto $postStoreDto,
        #[MapEntity(Post::class)] $post
    ): JsonResponse {
        $post = $this->postService->update($post, $postStoreDto, $this->getUser());

        return new JsonResponse((new PostResource())->item($post));
    }
}
