<?php

namespace App\Controller;

use App\Entity\User;
use App\Request\Dto\ModerationApproveDto;
use App\Request\Dto\ModerationBanDto;
use App\Request\Dto\UserUpdateDto;
use App\Response\PostResource;
use App\Response\UserProfileResource;
use App\Service\ModerationService;
use App\Service\UserProfileService;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/profile', name: 'profile_')]
final class ProfileController extends AbstractController
{
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly UserProfileService $userProfileService
    ) {
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    #[IsGranted('edit', 'user')]
    public function show(
        #[MapEntity(User::class)] User $user
    ): JsonResponse {
        return new JsonResponse($this->serializer->encode((new UserProfileResource())->item($user), 'json'));
    }

    #[Route('/{id}', name: 'edit', methods: ['PATCH'])]
    #[IsGranted('edit', 'user')]
    public function edit(
        #[MapRequestPayload] UserUpdateDto $dto,
        #[MapEntity(User::class)] $user
    ): JsonResponse {
        $this->userProfileService->update($user, $dto);

        return new JsonResponse($this->serializer->encode((new UserProfileResource())->item($user), 'json'));
    }
}
