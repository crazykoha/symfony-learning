<?php

namespace App\Controller;

use App\Entity\User;
use App\Request\Dto\ModerationApproveDto;
use App\Request\Dto\ModerationBanDto;
use App\Service\ModerationService;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/moderation', name: 'moderation_')]
final class ModerationController extends AbstractController
{
    public function __construct(
        private ModerationService $moderationService
    ) {
    }

    #[Route('/set-role/{id}', name: 'set_role', methods: ['POST'])]
    #[IsGranted('moderate')]
    public function setRole(
        #[MapRequestPayload] ModerationApproveDto $dto,
        #[MapEntity(User::class)] $user
    ): JsonResponse {
        $this->moderationService->setRole($user, $dto->role);

        return new JsonResponse('Accepted');
    }

    #[Route('/ban/{id}', name: 'ban', methods: ['POST'])]
    #[IsGranted('moderate')]
    public function ban(
        #[MapRequestPayload] ModerationBanDto $dto,
        #[MapEntity(User::class)] $user
    ): JsonResponse {
        $this->moderationService->banUser($user, $dto->ban_reason_id);

        return new JsonResponse('Accepted');
    }
}
