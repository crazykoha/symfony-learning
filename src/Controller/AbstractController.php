<?php

namespace App\Controller;

use App\Response\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as SymfonyController;
use Symfony\Component\HttpFoundation\JsonResponse;

class AbstractController extends SymfonyController
{
    protected function paginatedJson(
        Paginator $paginator,
        int $status = 200,
        array $headers = [],
        array $context = []
    ): JsonResponse {
        $result = $this->container->get('serializer')->serialize([
            'data' => $paginator->getQuery()->getResult(),
            'pagination' => [
                'page' => $paginator->getQuery()->getFirstResult() + 1,
                'per_page' => $paginator->getQuery()->getMaxResults(),
                'total' => $paginator->getTotal(),
                'total_pages' => (int) ceil($paginator->getTotal() / $paginator->getQuery()->getMaxResults()),
            ],
        ],
            'json',
            array_merge([
                'json_encode_options' => JsonResponse::DEFAULT_ENCODING_OPTIONS,
            ], $context));

        return new JsonResponse($result, $status, $headers, true);
    }
}
