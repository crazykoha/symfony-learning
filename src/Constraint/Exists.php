<?php

namespace App\Constraint;

use App\Validator\ExistsValidator;
use Symfony\Component\Validator\Attribute\HasNamedArguments;
use Symfony\Component\Validator\Constraint;

#[\Attribute]
class Exists extends Constraint
{
    public string $target;
    public string $message = 'The string "{{ string }}" contains an illegal character: it can only contain letters or numbers.';

    #[HasNamedArguments]
    public function __construct(string $target, mixed $options = null, array $groups = null, mixed $payload = null)
    {
        $this->target = $target;
        parent::__construct($options, $groups, $payload);
    }

    public function validatedBy()
    {
        return ExistsValidator::class;
    }
}