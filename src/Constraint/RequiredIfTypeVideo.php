<?php

namespace App\Constraint;

use App\Validator\RequiredIfTypeVideoValidator;
use Symfony\Component\Validator\Constraint;

#[\Attribute]
class RequiredIfTypeVideo extends Constraint
{
    public string $message = 'Данное значение значение не может быть позднее {{ compared_value }}.';

    public function validatedBy()
    {
        return RequiredIfTypeVideoValidator::class;
    }
}
