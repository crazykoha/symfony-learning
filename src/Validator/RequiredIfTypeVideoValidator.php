<?php

namespace App\Validator;

use App\Entity\PostType;
use App\Request\Dto\PostStoreDto;
use App\Request\Dto\PostUpdateDto;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class RequiredIfTypeVideoValidator extends ConstraintValidator
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function validate(mixed $value, Constraint $constraint)
    {
        /** @var PostStoreDto|PostUpdateDto $root */
        $root = $this->context->getRoot();
        $postType = $this->entityManager->getRepository(PostType::class)->find($root->post_type_id);
        if ('video' === $postType->getName() && empty($value)) {
            $this->context->addViolation('This property is required if post type is video.');
        }
    }
}
