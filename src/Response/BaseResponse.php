<?php

namespace App\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

abstract class BaseResponse
{
    public function paginatedCollection(Paginator $paginator): array
    {
        $data = [];
        foreach ($paginator->getQuery()->getResult() as $item) {
            $data[] = $this->toArray($item);
        }

        return [
            'data' => $data,
            'pagination' => [
                'page' => $paginator->getQuery()->getFirstResult() + 1,
                'per_page' => $paginator->getQuery()->getMaxResults(),
                'total' => $paginator->getTotal(),
                'total_pages' => (int) ceil($paginator->getTotal() / $paginator->getQuery()->getMaxResults()),
            ],
        ];
    }

    public function item(mixed $item): array
    {
         return $this->toArray($item);
    }

    protected abstract function toArray(mixed $item): array;
}