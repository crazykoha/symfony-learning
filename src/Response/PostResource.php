<?php

namespace App\Response;

use App\Entity\Post;

class PostResource extends BaseResponse
{
    /**
     * @param Post $item
     */
    protected function toArray(mixed $item): array
    {
        return [
            'id' => $item->getId(),
            'title' => $item->getTitle(),
            'context' => $item->getContext(),
            'body' => $item->getBody(),
            'author' => $item->getAuthor()->getUsername(),
            'type' => $item->getType()->getName(),
            'created_at' => $item->getCreatedAt()->format('d-m-Y'),
            'updated_at' => $item->getUpdatedAt()->format('d-m-Y'),
            'updated_by' => $item->getUpdatedBy()?->getUsername(),
        ];
    }
}
