<?php

namespace App\Response;

use App\Entity\Post;
use App\Entity\User;

class UserProfileResource extends BaseResponse
{
    /**
     * @param User $item
     */
    protected function toArray(mixed $item): array
    {
        return [
            'username' => $item->getUsername(),
            'email' => $item->getEmail(),
            'first_name' => $item->getFirstName(),
            'last_name' => $item->getLastName(),
            'role' => $item->getRole(),
            'ban_reason' => $item->getBanReason()?->getName(),
            'created_at' => $item->getCreatedAt()->format('d-m-Y'),
            'updated_at' => $item->getUpdatedAt()->format('d-m-Y'),
        ];
    }
}
