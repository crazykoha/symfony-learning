<?php

namespace App\Response;

use App\Entity\Post;

class PostCollection extends BaseResponse
{
    /**
     * @param Post $item
     */
    protected function toArray(mixed $item): array
    {
        return [
            'title' => $item->getTitle(),
            'context' => $item->getContext(),
            'author' => $item->getAuthor()->getUsername(),
            'created_at' => $item->getCreatedAt()->format('d-m-Y'),
        ];
    }
}
