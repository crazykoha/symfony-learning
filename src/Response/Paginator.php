<?php

namespace App\Response;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;

class Paginator extends DoctrinePaginator
{
    private int $total;

    public function __construct(Query $query, int $total, $fetchJoinCollection = true)
    {
        parent::__construct($query, $fetchJoinCollection);
        $this->total = $total;
    }

    public function getTotal(): int
    {
        return $this->total;
    }
}
