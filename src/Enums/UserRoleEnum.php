<?php

namespace App\Enums;

enum UserRoleEnum: string
{
    case Author = 'author';
    case Moderator = 'moderator';
    case Admin = 'admin';
}
