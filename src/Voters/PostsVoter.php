<?php

namespace App\Voters;

use App\Entity\Post;
use App\Entity\User;
use App\Enums\UserRoleEnum;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PostsVoter extends Voter
{
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!in_array($attribute, ['create', 'edit'])) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        return match ($attribute) {
            'create' => $this->canCreate($user),
            'edit' => $this->canEdit($user),
            default => throw new \LogicException('This code should not be reached!')
        };
    }

    private function canCreate(User $user): bool
    {
        return $user->getRole() === UserRoleEnum::Author->value;
    }

    private function canEdit(User $user): bool
    {
        return $this->canCreate($user);
    }
}
