<?php

namespace App\Voters;

use App\Entity\User;
use App\Enums\UserRoleEnum;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserProfileVoter extends Voter
{
    protected function supports(string $attribute, mixed $subject): bool
    {
        if ($subject instanceof User && 'edit' === $attribute) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if ($user->getId() === $subject->getId()) {
            return true;
        }

        return match ($user->getRole()) {
            null, 'author' => false,
            'moderator' => 'moderator' !== $subject->getRole() && 'admin' !== $subject->getRole(),
            'admin' => 'admin' !== $subject->getRole(),
            default => throw new \LogicException('This code should not be reached!')
        };
    }

    private function canCreate(User $user): bool
    {
        return $user->getRole() === UserRoleEnum::Author->value;
    }

    private function canEdit(User $user): bool
    {
        return $this->canCreate($user);
    }
}
