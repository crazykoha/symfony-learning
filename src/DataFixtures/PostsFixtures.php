<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\PostType;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PostsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $postType = new PostType();
        $postType->setName('Video');
        $manager->persist($postType);
        $user = new User(
            'test9@test.test',
            'test',
            'password'
        );
        $manager->persist($user);
        for ($i = 0; $i < 10; ++$i) {
            $post = new Post();
            $post->setTitle('Title');
            $post->setContext('Context');
            $post->setBody('Body');
            $post->setAuthor($user);
            $post->setType($postType);
            $manager->persist($post);
        }
        $manager->flush();
    }
}
