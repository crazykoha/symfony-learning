<?php

namespace App\Repository;

use App\Entity\BanReason;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BanReason>
 *
 * @method BanReason|null find($id, $lockMode = null, $lockVersion = null)
 * @method BanReason|null findOneBy(array $criteria, array $orderBy = null)
 * @method BanReason[]    findAll()
 * @method BanReason[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BanReasonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BanReason::class);
    }

//    /**
//     * @return BanReason[] Returns an array of BanReason objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?BanReason
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
