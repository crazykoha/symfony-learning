<?php

namespace App\Entity;

use App\Repository\RefreshTokenRepository;
use Doctrine\ORM\Mapping as ORM;
use Gesdinet\JWTRefreshTokenBundle\Model\AbstractRefreshToken;

#[ORM\Entity(repositoryClass: RefreshTokenRepository::class)]
#[ORM\Table(name: 'refresh_token')]
class RefreshToken extends AbstractRefreshToken
{
    #[
        ORM\Id,
        ORM\GeneratedValue,
        ORM\Column(type:"integer")
    ]
    protected $id;

    #[
        ORM\Column(type: "string", length: 128, nullable: true)
    ]
    protected $refreshToken;

    #[
        ORM\Column(type:"string", length: 255, nullable: true)
    ]
    protected $username;

    #[
        ORM\Column(type:"datetime", nullable: true)
    ]
    protected $valid;
}
