<?php

namespace App\Tests\Functional;

use ApiPlatform\Api\UrlGeneratorInterface;
use App\DataFixtures\PostsFixtures;
use App\Entity\Post;
use App\Entity\PostType;
use App\Entity\User;
use App\Enums\UserRoleEnum;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class PostsTest extends WebTestCase
{
    private Serializer $serializer;
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->client->enableProfiler();

        $databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
        $databaseTool->loadFixtures([PostsFixtures::class]);

        $this->serializer = static::getContainer()->get(SerializerInterface::class);
        $this->urlGenerator = static::getContainer()->get(UrlGeneratorInterface::class);
    }

    public function testUserCanGetListOfPosts(): void
    {
        $this->client->loginUser($this->createUser())->jsonRequest('GET', $this->urlGenerator->generate('api_posts'), ['page' => 1]);
        $response = $this->client->getResponse()->getContent();
        $response = $this->serializer->decode($response, 'json');
        $response = $this->serializer->decode($response, 'json');
        $this->assertCount(5, $response['data']);

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testUserCanGetSinglePost(): void
    {
        $post = static::getContainer()->get(EntityManagerInterface::class)->getRepository(Post::class)->findOneBy([]);

        $this->client->loginUser($this->createUser())->request('GET', $this->urlGenerator->generate('api_post', ['id' => $post->getId()]), [
            'headers' => [
                'Content-type' => 'application/json',
            ],
            'query' => [
                'page' => 1,
            ],
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testUserCanCreatePost(): void
    {
        $postType = new PostType();
        $postType->setName('ggg');

        $objectManager = static::getContainer()->get(EntityManagerInterface::class);
        $objectManager->persist($postType);
        $objectManager->flush();

        $user = $this->createUser();
        $this->client->loginUser($user)->jsonRequest('POST', $this->urlGenerator->generate('api_create_post'), [
            'title' => 'rrr',
            'context' => '',
            'body' => 'test',
            'post_type_id' => $postType->getId(),
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
        $post = static::getContainer()->get(PostRepository::class)->findOneBy(['title' => 'rrr']);
        $this->assertEquals('rrr', $post->getTitle());
        $this->assertEquals('test', $post->getBody());
        $this->assertEquals('te', $post->getContext());
        $this->assertEquals($postType->getId(), $post->getType()->getId());
        $this->assertEquals($user->getId(), $post->getAuthor()->getId());
    }

    public function testUserCanUpdatePost(): void
    {
        $post = static::getContainer()->get(EntityManagerInterface::class)->getRepository(Post::class)->findOneBy([]);

        $this->client->loginUser($this->createUser())->jsonRequest('PATCH', $this->urlGenerator->generate('api_update_post', ['id' => $post->getId()]), [
            'title' => 'rrr',
            'context' => '434',
            'body' => 'ttt',
            'post_type_id' => $post->getType()->getId(),
        ]);



        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $post = static::getContainer()->get(PostRepository::class)->findOneBy(['title' => 'rrr']);
        $this->assertEquals('rrr', $post->getTitle());
        $this->assertEquals('ttt', $post->getBody());
        $this->assertEquals('434', $post->getContext());
    }

    private function createUser(): User
    {
        $user = new User(
            'test11@test.test',
            'test',
            'password',
            role: UserRoleEnum::Author->value
        );
        $objectManager = static::getContainer()->get(EntityManagerInterface::class);
        $objectManager->persist($user);
        $objectManager->flush();

        return $user;
    }
}
