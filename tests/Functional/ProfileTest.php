<?php

namespace App\Tests\Functional;

use ApiPlatform\Api\UrlGeneratorInterface;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ProfileTest extends WebTestCase
{
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;
    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->urlGenerator = static::getContainer()->get(UrlGeneratorInterface::class);
        $this->entityManager = static::getContainer()->get(EntityManagerInterface::class);
    }

    public function testAdminCanSeeProfile(): void
    {
        $user = $this->createUser('moderator');
        $this->client
            ->loginUser($this->createUser('admin'))
            ->jsonRequest('GET', $this->urlGenerator->generate('profile_show', ['id' => $user->getId()]));
        $response = $this->client->getResponse()->getContent();
        $response = json_decode($response);
        $response = json_decode($response, true);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertArrayHasKey('username', $response);
        $this->assertArrayHasKey('email', $response);
        $this->assertArrayHasKey('first_name', $response);
        $this->assertArrayHasKey('last_name', $response);
        $this->assertArrayHasKey('role', $response);
        $this->assertArrayHasKey('ban_reason', $response);
        $this->assertArrayHasKey('created_at', $response);
        $this->assertArrayHasKey('updated_at', $response);
    }

    public function testAdminCanEditProfile(): void
    {
        $user = $this->createUser('moderator');
        $this->client
            ->loginUser($this->createUser('admin'))
            ->jsonRequest('PATCH', $this->urlGenerator->generate('profile_edit', ['id' => $user->getId()]), [
                'first_name' => 'test1',
                'last_name' => 'test2',
            ]);
        $this->entityManager->refresh($user);
        $this->assertEquals('test1', $user->getFirstName());
        $this->assertEquals('test2', $user->getLastName());
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    private function createUser(?string $role): User
    {
        $faker = Factory::create();
        $user = new User(
            $faker->email(),
            $faker->userName(),
            $faker->password(),
            $faker->firstName(),
            $faker->lastName(),
            $role
        );
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
