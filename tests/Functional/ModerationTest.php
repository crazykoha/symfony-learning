<?php

namespace App\Tests\Functional;

use ApiPlatform\Api\UrlGeneratorInterface;
use App\Entity\BanReason;
use App\Entity\User;
use App\Enums\UserRoleEnum;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ModerationTest extends WebTestCase
{
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;
    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->urlGenerator = static::getContainer()->get(UrlGeneratorInterface::class);
        $this->entityManager = static::getContainer()->get(EntityManagerInterface::class);

    }

    public function testAdminCanApproveUser(): void
    {
        $userToApprove = $this->createUser();
        $this->client->loginUser($this->createUser())->jsonRequest('POST', $this->urlGenerator->generate('moderation_set_role', ['id' => $userToApprove->getId()]), ['role' => 'author']);
        $this->entityManager->refresh($userToApprove);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertEquals('author', $userToApprove->getRole());
    }

    public function testAdminCanBanUser(): void
    {
        $banReason = new BanReason();
        $banReason->setName('test');
        $this->entityManager->persist($banReason);
        $this->entityManager->flush();
        $userToBan = $this->createUser();
        $this->client->loginUser($this->createUser())->jsonRequest('POST', $this->urlGenerator->generate('moderation_ban', ['id' => $userToBan->getId()]), ['ban_reason_id' => $banReason->getId()]);
        $this->entityManager->refresh($userToBan);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertEquals($banReason->getId(), $userToBan->getBanReason()->getId());
    }

    private function createUser(): User
    {
        $faker = Factory::create();
        $user = new User(
            $faker->email(),
            $faker->userName(),
            $faker->password(),
            role: UserRoleEnum::Admin->value
        );
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
