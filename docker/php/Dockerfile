FROM composer:2.6.2 as composer

##################################

FROM php:8.2-fpm-alpine3.18
    
RUN apk add --no-cache \
    bash=~5.2 \
    git=~2.40 \
    icu-dev=~73.2

RUN set -ex \
  && apk --no-cache add \
    postgresql-dev


RUN mkdir -p /usr/src/app \
    && apk add --no-cache --virtual=.build-deps \
        autoconf=~2.71 \
        g++=~12.2 \
    && docker-php-ext-configure intl \
    && docker-php-ext-install -j"$(nproc)" intl pdo pdo_pgsql \
    && docker-php-ext-enable intl \
    && apk del .build-deps

RUN apk add --no-cache autoconf g++ make linux-headers\
    && pecl install xdebug \
    && docker-php-ext-enable xdebug

RUN echo "xdebug.client_port=9001" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini


WORKDIR /usr/src/app

COPY composer.json /usr/src/app/composer.json
COPY composer.lock /usr/src/app/composer.lock
    
RUN PATH=$PATH:/usr/src/app/vendor/bin:bin

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN composer install --no-scripts

COPY ./ /usr/src/app

RUN chown -R 1000:1000 /usr/src/app
USER 1000:1000